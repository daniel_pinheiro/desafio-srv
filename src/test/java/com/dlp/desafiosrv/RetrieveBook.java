package com.dlp.desafiosrv;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.web.client.RestTemplate;

import com.dlp.desafiosrv.util.WebUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RetrieveBook {
	
	public static void main(String args[]) throws ParseException {
        WebUtils wu = new WebUtils();
        ObjectMapper objectMapper = new ObjectMapper();
        
        URL url;
		try {
			url = new URL(wu.getBookBySkuApi("443852"));
			JsonNode node  = objectMapper.readValue(url,  JsonNode.class);
			NumberFormat nf = NumberFormat.getNumberInstance(Locale.FRANCE);
			System.out.println(node.get("sku").asInt()+ "-"+nf.parse(node.get("price").get("value").asText()));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        
       
       // log.info(quote.toString());
    }

}
