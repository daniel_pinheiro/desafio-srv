package com.dlp.desafiosrv.book;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dlp.desafiosrv.util.WebUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * 
 * @author Daniel Pinheiro
 *
 */
@RestController
@RequestMapping("/book/")
public class BookRestController {

	@Autowired
	private BookRepository bookRepository;
//	@Autowired
//	private BookService bookService;
	

	
	@PostMapping()
	ResponseEntity<?> add( @RequestParam String sku) {
        ObjectMapper objectMapper = new ObjectMapper();
        WebUtils wu = new WebUtils();
        URL url;
        System.out.println("Create: "+sku);
		try {
			 Optional<Book> res = bookRepository.findById(Integer.parseInt(sku));
			 if (res.isPresent())
			 {
				return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
			 } else {
				url = new URL(wu.getBookBySkuApi(sku));
				JsonNode node  = objectMapper.readValue(url,  JsonNode.class);
				NumberFormat nf = NumberFormat.getNumberInstance(Locale.FRANCE);
				Book book = new Book();
				book.setSku(node.get("sku").asInt());
				book.setPrice(nf.parse(node.get("price").get("value").asText()).doubleValue());
				book.setBrand(node.get("brand").asText());
				book.setName(node.get("name").asText());
				bookRepository.save(book);
				return new ResponseEntity<String>(HttpStatus.CREATED.getReasonPhrase()+" 201 \n", HttpStatus.CREATED);
			 }
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<String>(HttpStatus.CREATED); 
	}
	@DeleteMapping("/{sku}")
	public String remove( @PathVariable int sku) {
        System.out.println("Delete: "+sku);
		try {
			 Optional<Book> res = bookRepository.findById(sku);
			 System.out.println(res.isPresent());
			 if (res.isPresent())
			 {
				bookRepository.delete(res.get());
				return HttpStatus.NO_CONTENT.getReasonPhrase()+" 204 \n";
			 } else {
				return HttpStatus.NOT_MODIFIED.getReasonPhrase()+" 304 \n";
			 }
			
		} finally {
			
		}
	}
	@GetMapping("/{sku}")
	public ResponseEntity<?> get( @PathVariable int sku) {
        System.out.println("GetBySku: "+sku);
		try {
			 Optional<Book> res = bookRepository.findById(sku);
			 System.out.println(res.isPresent());
			 if (res.isPresent())
			 {
				return new ResponseEntity<Book>(res.get(), HttpStatus.OK);
			 } else {
				return new ResponseEntity<String>(HttpStatus.NOT_FOUND.getReasonPhrase()+"404 \n", HttpStatus.NOT_FOUND);
			 }
			
		} finally {
			
		}
		
	}
		@GetMapping("/")
		public ResponseEntity<List<Book>> getFiltered( @RequestParam double price, @RequestParam int limit) {
	        
				 List<Book> res = bookRepository.findAll();
				// System.out.println(res.isPresent());
				 
				return new ResponseEntity<List<Book>>(res, HttpStatus.OK);
				
			
	}
	
}
