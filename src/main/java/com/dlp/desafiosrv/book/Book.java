package com.dlp.desafiosrv.book;

import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * 
 * @author danielpinheiro
 *
 */
@Entity
public class Book {
	
	@Id
	private Integer sku;
	private String name;
	private String brand;
	private Double price;
	
	public Integer getSku() {
		return sku;
	}
	public void setSku(Integer sku) {
		this.sku = sku;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

}
