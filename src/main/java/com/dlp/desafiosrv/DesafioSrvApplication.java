package com.dlp.desafiosrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioSrvApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioSrvApplication.class, args);
	}
	
}
