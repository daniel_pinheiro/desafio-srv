package com.dlp.desafiosrv.util;

import org.springframework.context.annotation.Bean;

public class WebUtils {

	private String urlSaraiva = "https://api.saraiva.com.br/sc/produto/pdp/%s/0/0/1/";
	@Bean
	public String getBookBySkuApi(String sku) {
		return sku != null && !sku.isEmpty() ? String.format(urlSaraiva, sku): "";
	}
}
